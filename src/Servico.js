import React from 'react';
import Footer from './components/footer/Footer.js'
import db from './config/FireStoreConfig'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Menu from './components/menu/Menu'

export default class Servico extends React.Component{
  constructor(props) {
  super(props)
  this.state={
    nome: '',
    descricao: '',
    duracao: ''
  }
  }
    
  salvarServico = () => {
    db.collection('servicos').add({
      nome:this.state.nome,
      descricao:this.state.descricao,
      duracao:this.state.duracao
    })
  }
    
  handleChangeNome = (e) => {
    this.setState({...this.state, nome : e.target.value})
  }
      
  handleChangeDescricao = (e) => {
    this.setState({...this.state, descricao : e.target.value})
  }
      
  handleChangeDuracao = (e) => {
    this.setState({...this.state, duracao : e.target.value})
  }
    
  render() {
    return (
      <>
      <Menu tela='Servico'/>
      <div style={{textAlign:'center'}}>
      
      <div>
        {/* <div>Nome</div> */}
        <TextField id="nome" label="nome" type="text" value={this.state.nome} onChange={this.handleChangeNome}/>
        {/* <input id="nome" placeholder="Digite o nome do serviço" type="text" value={this.state.nome} */}
        {/* onChange={this.handleChangeNome}/> */}
      </div>

      <div>
        {/* <div>Descrição</div> */}
        <TextField id="descrição" label="descricao" type="text" value={this.state.descricao} onChange={this.handleChangeDescricao}/>
        {/* <input id="descricao" placeholder="Digite a descrição do serviço" type="text" value={this.state.descricao}
        onChange={this.handleChangeDescricao}/> */}
      </div>

      <div>
        {/* <div>Duracao</div> */}
        <TextField id="duração" label="duração" type="text" value={this.state.duracao} onChange={this.handleChangeDuracao}/>
        {/* <input id="duracao" placeholder="Digite a duração do serviço" type="text" value={this.state.duracao}
        onChange={this.handleChangeDuracao}/> */}
      </div>

      <div>
        <small id="emailAjuda" className="form-text text-muted">**OBS: Não compartilharemos suas informações**</small>
      </div>

      <div>
        <Button variant="contained" style={{margin:'10px'}} onClick={this.props.history.goBack} color="default">Voltar</Button>
        {/* <Link to="/Home" style={{margin: '10px'}}>Voltar</Link>
        <button style={{margin:'10px'}} onClick={this.salvarServico}>Confirma</button> */}
        <Button variant="contained" style={{margin:'10px'}} onClick={this.salvarServico} color="default">Confirma</Button>

        {/* <button style={{margin:'10px'}} onClick={this.login}>Continuar</button>
        <button style={{margin:'10px'}} onClick={this.signUp}>Voltar</button> */}
        <Footer logoutpath='/'/>
      </div>
      </div>
    </>
    );
  }
}
