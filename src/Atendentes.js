import React from 'react';
import Footer from './components/footer/Footer.js'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Menu from './components/menu/Menu'
import db from './config/FireStoreConfig'

  export default class Atendente extends React.Component{
    constructor(props) {
      super(props)
      this.state={
        nome:'',
        servicos:[],
        servSelecionados:[],
        usuario:'',
        senha:''
      }
    }

    handleChangeMultiple = event => {
      const { value } = event.target;
      
      this.setState({...this.state, servSelecionados : value})
    };
    
    salvarAtendente = () => {
      db.collection('atendentes').add({
        nome:this.state.nome,
        senha:this.state.senha,
        servico:this.state.servSelecionados,
        usuario:this.state.usuario
      })
      .then(() => {
        
      })
    }
    
    handleChangeNome = (e) => {
      this.setState({...this.state, nome : e.target.value})
    }
    
    handleChangeServicos = (e) => {
      // console.log(this.state.servSelecionados)
      this.setState({...this.state, servSelecionados : [...this.state.servSelecionados, e.target.value]})
    }
    handleChangeUsuario = (e) => {
      this.setState({...this.state, usuario : e.target.value})
    }
    handleChangeSenha = (e) => {
      this.setState({...this.state, senha : e.target.value})
    }
    
    componentDidMount() {    
      let resultado = []
      db.collection('servicos').get()
      .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
        resultado.push({...doc.data(), id:doc.id})
        // console.log(doc.id, "=>", doc.data())
      })
    })
    .then(() => {
      this.setState({...this.state, servicos: resultado})
    })
  }
  
  // print = () => {
  //   console.log(this.state.servSelecionados)
  // }
  
  
  render() {
    const {servicos, servSelecionados} = this.state
    console.log(servicos)
    return (
      <>
        <Menu tela='Atendente'/>
        <div style={{textAlign:'center'}}>

          <div>
            {/* <div>nome</div> */}
            <TextField id="nome" label="nome" type="text" value={this.state.nome} onChange={this.handleChangeNome}/>
            {/* <input id="nome" placeholder="Digite o nome do serviço" type="text"/> */}
          </div>

          <div>
            <InputLabel style={{margin:'10px'}}>escolha um ou mais serviços</InputLabel>

            <Select
              id="servicos"
              multiple
              value={servSelecionados}
              onChange={this.handleChangeMultiple}>
              {servicos.map(servico => (
                <MenuItem key={servico.id} value={servico.id}>
                  {servico.nome}
                </MenuItem>
                ))} 
            </Select>

            {/* <Select
              multiple
              value={servSelecionados}
              onChange={this.handleChangeMultiple}
              style={{marginTop:'10px'}}>
              {servicos.map(servico => (
              <MenuItem key={servico} value={servico}>
                {servico}
            </MenuItem>
            ))}
          </Select> */}
          </div>
                
          <div>
            {/* <div>usuario</div> */}
            <TextField id="usuario" label="usuario" type="text" value={this.state.usuario} onChange={this.handleChangeUsuario}/>
            {/* <input id="usuario" placeholder="Digite a duração do serviço" type="text"/> */}
          </div>

          <div>
            {/* <div>senha</div> */}
            <TextField id="senha" label="senha" type="text" value={this.state.senha} onChange={this.handleChangeSenha}/>
            {/* <input id="senha" placeholder="Digite a duração do serviço" type="text"/> */}
          </div>


          <div>
            <small id="emailAjuda" className="form-text text-muted">**OBS: Não compartilharemos suas informações**</small>
          </div>

          <div>
          <Button variant="contained" style={{margin:'10px'}} onClick={this.props.history.goBack} color="default">Voltar</Button>
            {/* <Link to="/Home" style={{margin: '10px'}}>Voltar</Link> */}
            {/* <Link to="/" style={{margin: '10px'}}>Confirma</Link> */}
            <Button variant="contained" style={{margin:'10px'}} onClick={this.salvarAtendente} color="default">Confirma</Button>
            {/* <Button variant="contained" style={{margin:'10px'}} onClick={this.print} color="default">Print</Button> */}
            
            {/* <button style={{margin:'10px'}} onClick={this.login}>Continuar</button> */}
            {/* <button style={{margin:'10px'}} onClick={this.signUp}>Voltar</button> */}
            <Footer/>
          </div>
        </div>
      </>
    );
  }
}
// export default Atendentes;