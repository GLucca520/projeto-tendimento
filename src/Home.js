import React from 'react';
import fire from './config/Fire';
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import Footer from './components/footer/Footer.js'
import Menu from './components/menu/Menu'

class Home extends React.Component {
  //Colocar o usuario no estado
  constructor(props) {
    super(props)
    this.logout = this.logout.bind(this)
    // this.state={
    //   user:''
    // }
  }

  logout() {
    fire.auth().signOut()
  }
  
  render() {
    return (
    <div>
      <Menu tela='Home'/>

      {/* <div id="boasVindas" style={{textAlign:'center'}}>Bem vindo, </div> */}
      
      <div id="botao1">
      <Button component={Link} to="/ListarServico" variant="contained" style={{margin:'10px'}} color="primary">Servico</Button>
      {/* <Link to="/Servico">Servico</Link> */}
      </div>

      <div id="botao2">
      <Button component={Link} to="/Atendentes" variant="contained" style={{margin:'10px'}} color="primary">Atendentes</Button>
      {/* <Link to="/Atendentes">Atendentes</Link> */}
      </div>

      <div id="botao3">
      <Button component={Link} to="/Disponibilidade" variant="contained" style={{margin:'10px'}} color="primary">Disponibilidade</Button>
      {/* <Link to="/Disponibilidade">Disponibilidade</Link> */}
      </div>

      <div id="botao4">
      <Button component={Link} to="/Agenda" variant="contained" style={{margin:'10px'}} color="primary">Agendamento</Button>
      {/* <Link to="/Agenda">Agendamento</Link> */}
      </div>

      <Footer/>
    </div>
    );
  }
}

export default Home;
