import React from 'react'
import { Toolbar } from '@material-ui/core';
import './ToolBar.css'
const toolbar = props => (
  <header className="ToolBar">
    <nav className="ToolBarNavigation">
      <div></div>
      <div className="ToolBarLogo"><a href="/">The logo</a></div>
      <div className="ToolBarNavigationItems">
        <ul>
          <li><a href="/">Servico</a></li>
          <li><a href="/">Disponibilidade</a></li>
          <li><a href="/">Atendente</a></li>
          <li><a href="/">Agendamento</a></li>
          <li><a href="/">Login</a></li>
          <li><a href="/">Cadastro</a></li>
          <li><a href="/">Usuario</a></li>
        </ul>
      </div>
    </nav>
  </header>
);
export default Toolbar