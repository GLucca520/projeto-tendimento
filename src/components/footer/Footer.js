import React, {Component} from 'react';
import fire from '../../config/Fire';
import Button from '@material-ui/core/Button';

class Footer extends Component {
  constructor(props) {
    super(props)
    this.logout = this.logout.bind(this)

  }

  logout() {
      fire.auth().signOut()
    }
  
  render() {
    return (

      <div>
        <Button variant="contained" style={{margin:'10px'}} onClick={this.logout} color="default">Sair</Button>
      </div>
    );
  }
}
export default Footer;