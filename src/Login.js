import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import Menu from './components/menu/Menu';
import fire from './config/Fire';
import db from './config/FireStoreConfig'


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

class Login extends React.Component {
  //Colocar o usuario no estado
  constructor(props) {
    super(props)
    this.handleChangeEmail = this.handleChangeEmail.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.login = this.login.bind(this)
    this.state = {
      email : '',
      password : '',
    }
  }

    handleChangeEmail(e) {
      this.setState({...this.state, email : e.target.value})
    }

    handleChangePassword(e) {
      this.setState({...this.state, password : e.target.value})
    }

  // componentDidMount() {
  //   this.authListener();  
  // }

  //Fazer login
  login() {
    fire.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
    .then((u) => {
      alert("Logado com sucesso!")
    })
    .catch((err) => {
      console.log("Erro: " + err.toString());
      alert("Email ou senha incorretos! Porfavor, digite novamente")
    })
  }

  //Criação de usuário
  signUp = () => {
    fire.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
    .then((u) => {
        db.collection('usuarios').add({
          email:this.state.email,
          password:this.state.password,
        });
        Redirect('/Login')
      })
    .catch((err) => {
      console.log("Erro: " + err.toString());
    })
  }

  render() {
    return (
      <>
      <Menu tela="Login"/>
      {/* <Design/> */}
      <div style={{textAlign:'center'}}>
        
        <div>
          {/* <div>email</div> */}
          <TextField id="email" label="email" type="text" value={this.state.email} onChange={this.handleChangeEmail}/>
        </div>

        <div>
          {/* <div>senha</div> */}
          <TextField id="password" label="senha" type="password" value={this.state.password} onChange={this.handleChangePassword}/>
          {/* <input id="password" placeholder="Digite sua senha" type="password" value={this.state.password} onChange={this.handleChangePassword}/> */}
        </div>

        <div>
          <small id="emailAjuda" className="form-text text-muted">**OBS: Não compartilharemos suas informações**</small>
        </div>

        <div>

        <Button variant="contained" style={{margin:'10px'}} onClick={this.login} color="default">Login</Button>
          {/* <button style={{margin:'10px'}} onClick={this.login}>Login</button> */}
          {/* <Button component={Link} to="/Cadastro" variant="contained" style={{margin:'10px'}} onClick={this.signUp} color="default">Cadastre-se</Button> */}

        <div>
          <Link to="/Cadastro" style={{margin: '10px'}}>Ainda não tem uma conta? Cadastre-se</Link>
        </div>

        </div>

        <Copyright/>

      </div>
      </>
    );
  }
}
export default Login;
