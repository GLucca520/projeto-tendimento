import React from 'react'
import fire from './config/Fire'
import db from './config/FireStoreConfig'
import {Redirect} from 'react-router'
import Menu from './components/menu/Menu'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

class Cadastro extends React.Component{
  
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      redirect: false
    }
  }   
  
  //Criação de usuário
  signUp = () => {
    fire.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
    .then((u) => {
        db.collection('usuarios').add({
          email:this.state.email,
          password:this.state.password,
          nome:this.state.nome,
        });
        alert('Cadastrado com sucesso')
        Redirect('/Home')
      })
    .catch((err) => {
      console.log("Erro: " + err.toString());
    })
  }
  
  handleChangeEmail = (e) => {
    this.setState({...this.state, email : e.target.value})
  }
  
  handleChangePassword = (e) => {
    this.setState({...this.state, password : e.target.value})
  }

  handleChangeNome = (e) => {
    this.setState({...this.state, nome : e.target.value})
  }
  
  render() {
    return (
      <>
      <Menu tela='Cadastro'/>
      <div style={{textAlign:'center'}}>

        <div>
          <TextField id="nome" label="nome" type="text" value={this.state.nome} onChange={this.handleChangeNome}/>
        </div>
      
        <div>
          {/* <div>email</div> */}
          <TextField id="email" label="email" type="text" value={this.state.email} onChange={this.handleChangeEmail}/>
          {/* <input id="email" placeholder="Digite um email para acesso" type="text" value={this.state.email} onChange={this.handleChangeEmail}/> */}
        </div> 

        <div>
          {/* <div>senha</div> */}
          <TextField id="password" label="senha" type="password" value={this.state.password} onChange={this.handleChangePassword}/>
          {/* <input id="password" placeholder="Digite uma senha de acesso" type="password" value={this.state.password} onChange={this.handleChangePassword}/> */}
        </div>

        <div>
          <small id="emailAjuda" className="form-text text-muted">**OBS: Não compartilharemos suas informações**</small>
        </div>

        <div>
          <Button variant="contained" style={{margin:'10px'}} onClick={this.props.history.goBack} color="default">Voltar</Button>
          <Button variant="contained" style={{margin:'10px'}} onClick={this.signUp} color="default">Confirma</Button>
          {/* <button style={{margin:'10px'}} onClick={this.signUp}>Confirma</button> */}
        </div>

      </div>
      </>
    )
  }
}
export default Cadastro;