import React from 'react';
import Footer from './components/footer/Footer.js'
import Button from '@material-ui/core/Button';
import Menu from './components/menu/Menu'
import TextField from '@material-ui/core/TextField';
import db from './config/FireStoreConfig'
import Select from '@material-ui/core/Select';
import { InputLabel, MenuItem } from '@material-ui/core';

// const useStyles = makeStyles(theme => ({
//   root: {
//     '& > *': {
//       margin: theme.spacing(1),
//     },
//   },
// }));


class Servico extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      diasDaSemana:'',
      horaInicial:'',
      horaFinal:'',
      atendentes:[],
      atendSelecionado:'',
    }
  }
  
  salvarDisponibilidade = () => {
    db.collection('disponibilidade').add({
      diasDaSemana:this.state.diasDaSemana,
      horaInicial:this.state.horaInicial,
      horaFinal:this.state.horaFinal,
      atendente:this.state.atendSelecionado
    })
  }

  handleChangediasDaSemana = (e) => {
    this.setState({...this.state, diasDaSemana : e.target.value})
  }
  
  handleChangeHoraInicial = (e) => {
    this.setState({...this.state, horaInicial : e.target.value})
  }
  handleChangeHoraFinal = (e) => {
    this.setState({...this.state, horaFinal : e.target.value})
  }
  
  handleChangeAtendente = (e) => {
    this.setState({...this.state, atendSelecionado : e.target.value})
  }

  componentDidMount() {
    let resultado = []
      db.collection('atendentes').get()
      .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
          resultado.push({...doc.data(), id:doc.id})
        })
      })
      .then(() => {
        this.setState({...this.state, atendentes: resultado})
      })}

  render() {
    const {atendSelecionado, atendentes} = this.state
    console.log(atendentes)
    return (
    <>
    <Menu tela='Disponibilidade'/>
      <div style={{textAlign:'center', margin:'10px'}}>

        <div>
          {/* <div>dias da semana</div> */}
          <TextField id="diasDaSemana" label="dias da semana" type="text" value={this.state.email} onChange={this.handleChangediasDaSemana}/>
          {/* <input id="diasDaSemana" placeholder="Digite os dias da semana" type="text"/> */}
        </div>

        <div>
          {/* <div>hora Inicial</div> */}
          <TextField id="horaInicial" label="hora inicial" type="text" value={this.state.email} onChange={this.handleChangeHoraInicial}/>
          {/* <input id="horaInicial" placeholder="Digite a descrição do serviço" type="text"/> */}
        </div>

        <div>
          {/* <div>hora final</div> */}
          <TextField id="horaFinal" label="hora final" type="text" value={this.state.email} onChange={this.handleChangeHoraFinal}/>
          {/* <input id="horaFinal" placeholder="Digite a duração do serviço" type="text"/> */}
        </div>

        <div>
            <InputLabel style={{margin:'10px'}}>Atendente</InputLabel>

            <Select
            id="atendente"
            value={atendSelecionado}
            onChange={this.handleChangeAtendente}>
              {atendentes.map(atendente =>(
                <MenuItem key={atendente.id} value={atendente.id}>
                  {atendente.nome}
                </MenuItem>
              ))}
            </Select>

          {/* <div>atendente</div> */}
          {/* <TextField id="atendente" label="atendente" type="text" value={this.state.email} onChange={this.handleChangeAtendente}/> */}
          {/* <input id="atendente" placeholder="Digite a duração do serviço" type="text"/> */}
        </div>

        <div>
          <small id="emailAjuda" className="form-text text-muted">**OBS: Não compartilharemos suas informações**</small>
        </div>

        <div>
          <Button variant="contained" style={{margin:'10px'}} onClick={this.props.history.goBack} color="default">Voltar</Button>
          {/* <Link to="/Atendentes" style={{margin: '10px'}}>Confirma</Link>
          <Link to="/Home" style={{margin: '10px'}}>Avançar</Link> */}
          <Button variant="contained" style={{margin:'10px'}} onClick={this.salvarDisponibilidade} color="default">Confirma</Button>
          {/* <button style={{margin:'10px'}} onClick={this.login}>Continuar</button>
          <button style={{margin:'10px'}} onClick={this.signUp}>Voltar</button> */}
          <Footer logoutpath='/' />
        </div>
      </div>
      </>
    );
  }
}
export default Servico;