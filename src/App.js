import React, {Component} from 'react';
import Home from './Home.js'
import Login from './Login.js'
import fire from './config/Fire.js'; 

class App extends Component {
  //Colocar o usuario no estado
  constructor(props) {
    super(props);
    this.state={
        user:null,
    }

    this.authListener = this.authListener.bind(this)
 }

  componentDidMount() {
    this.authListener();  
  }


  //É chamado quando o estado de autenticação altera
  authListener() {
    fire.auth().onAuthStateChanged((user) => {
      if(user) {
        this.setState({user})
      } else {
        this.setState({user: null});
      }
    });
  }

    render() {
      return (
        <div>
          {this.state.user ? (<Home/>) : (<Login/>)}
        </div>
      );
  }
}
export default App;
