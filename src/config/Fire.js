import firebase from 'firebase';
const firebaseConfig = {
  apiKey: "AIzaSyARJp4619NTFzrb1-ZK-6kzq2mXwhdJcuc",
  authDomain: "atendimento-bbe26.firebaseapp.com",
  databaseURL: "https://atendimento-bbe26.firebaseio.com",
  projectId: "atendimento-bbe26",
  storageBucket: "atendimento-bbe26.appspot.com",
  messagingSenderId: "792324319627",
  appId: "1:792324319627:web:ae966b7ef701a7b0d9e78e",
  measurementId: "G-SH7LY168C4"
};
const fire = firebase.initializeApp(firebaseConfig);
export default fire;