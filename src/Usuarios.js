import React from 'react';
import {Link} from 'react-router-dom'
import Footer from './components/footer/Footer.js'

// arrayUsuarios = () =>{
//   title: "Usuarios",
//   categories: {
//     "nome":true,
//     "email":true,
//     "senha":true,
//   }
//   }  

class Servico extends React.Component{

  render() {
    return (
      <div style={{textAlign:'center'}}>
        
        <div>
          <div>id</div>
          <input id="id" placeholder="Digite o id so serviço" type="text"/>
        </div>

        <div>
          <div>nome</div>
          <input id="nome" placeholder="Digite o nome do serviço" type="text"/>
        </div>

        <div>
          <div>email</div>
          <input id="email" placeholder="Digite a email do serviço" type="text"/>
        </div>

        <div>
          <div>senha</div>
          <input id="password" placeholder="Digite a senha do serviço" type="text"/>
        </div>

        <div>
          <small id="emailAjuda" class="form-text text-muted">**OBS: Não compartilharemos suas informações**</small>
        </div>

        <div>
          <Link to="/" style={{margin: '10px'}}>Voltar</Link>
          <Link to="/atendente" style={{margin: '10px'}}>Avançar</Link>
          {/* <button style={{margin:'10px'}} onClick={this.login}>Continuar</button>
          <button style={{margin:'10px'}} onClick={this.signUp}>Voltar</button> */}
          <Footer logoutpath='/' />
        </div>
      </div>
    );
  }
}
export default Servico;