import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Servico from './Servico';
import Cadastro  from './Cadastro'
import Login  from './Login'
import Atendentes from './Atendentes'
import Disponibilidade  from './Disponibilidade'
import Usuarios  from './Usuarios'
import Agenda  from './Agenda'
import ListarServico from './ListarServico'
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Switch, Route} from 'react-router-dom'

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" exact={true} component={App}/>
      <Route path="/Home" exact={true} component={App}/>
      <Route path="/Servico" component={Servico}/>
      <Route path="/Cadastro" component={Cadastro}/>
      <Route path="/Login" component={Login}/>
      <Route path="/Atendentes" component={Atendentes}/>
      <Route path="/Disponibilidade" component={Disponibilidade}/>
      <Route path="/Usuarios" component={Usuarios}/>
      <Route path="/Agenda" component={Agenda}/>
      <Route path="/ListarServico" component={ListarServico}/>
    </Switch>
  </BrowserRouter>
  ,document.getElementById('root')
);
serviceWorker.unregister();
