import React from 'react';
import Footer from './components/footer/Footer.js'
import Menu from './components/menu/Menu'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';    
import db from './config/FireStoreConfig.js';
import { InputLabel, MenuItem } from '@material-ui/core';
import Select from '@material-ui/core/Select';

class Servico extends React.Component{
  constructor(props) {
    super(props)
    this.state={
      usuario:'',
      atendentes:[],
      atendSelecionado:'',
      dataInicio:'',
      servSelecionado:'',
      servicos:[],
      dataFinal:'',
      avaliacao:''
    }
  }

  salvarAgenda = () => {
    db.collection('agendamento').add({
      usuario:this.state.usuario,
      atendente:this.state.atendSelecionado,
      dataInicio:this.state.dataInicio,
      servico:this.state.servSelecionado,
      dataFinal:this.state.dataFinal,
      avaliacao:this.state.avaliacao
    })
  }

  handleChangeMultiple = event => {
    const { value } = event.target;

    this.setState({...this.state, servSelecionado : value})
  }
  handleChangeUsuario = (e) => {
    this.setState({...this.state, usuario : e.target.value})
  }
  handleChangeAtendente = (e) => {
    this.setState({...this.state, atendSelecionado : e.target.value})
  }
  handleChangeDataInicio = (e) => {
    this.setState({...this.state, dataInicio : e.target.value})
  }
  handleChangeServicos = (e) => {
    this.setState({...this.state, servSelecionado : e.target.value})
  }
  handleChangeDataFinal = (e) => {
    this.setState({...this.state, dataFinal : e.target.value})
  }
  handleChangeAvaliacao = (e) => {
    this.setState({...this.state, avaliacao : e.target.value})
  }

    getAtendentes = () => {
    let resultado = []
    console.log(resultado)
    db.collection('atendentes').get()
    .then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
        resultado.push({...doc.data(), id:doc.id})
      })
    })
    .then(() => {
      this.setState({...this.state, atendentes: resultado})
    })
    .catch((err) => {
      console.log(err)
    })
    }

    getServicos = () => { 
    let resultado = []
    console.log(resultado)
    db.collection('servicos').get()
    .then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
        resultado.push({...doc.data(), id:doc.id})
        })
      })
      .then(() => {
        this.setState({...this.state, servicos: resultado})
    })
    }

    componentDidMount() {
      this.getAtendentes();
      this.getServicos();
    }

  render() {
    const {servicos, servSelecionado} = this.state
    const {atendentes, atendSelecionado} = this.state
    // console.log(atendentes)
    // console.log(servicos)
    return (
      <>
      <Menu tela='Agendamento'/>
      <div style={{textAlign:'center'}}>

        
        <div>
          {/* <div>usuario</div> */}
          <TextField id="usuario" label="usuário" type="text" value={this.state.email} onChange={this.handleChangeUsuario}/>
          {/* <input id="usuario" placeholder="Digite o id so serviço" type="text"/> */}
        </div>

        <div>
          <InputLabel id="demo-mutiple-name-label">Atendente</InputLabel>

          {/* <Select
              id="servicos"
              multiple
              value={servSelecionado}
              onChange={this.handleChangeMultiple}>
              {servicos.map(servico => (
                <MenuItem key={servico.id} value={servico.nome}>
                  {servico.nome}
                </MenuItem>
                ))}
            </Select> */}

          <Select
          id="atendentes"
          value={atendSelecionado}
          style={{marginTop:'10px'}}
          onChange={this.handleChangeAtendente}>
            {atendentes.map(atendente =>(
              <MenuItem key={atendente.id} value={atendente.id}>
                {atendente.nome}
              </MenuItem>
            ))}
          </Select>

          {/* <Select
          id="atendentes"
          style={{marginTop:'10px'}}
          value={atendentes}
          onChange={this.handleChangeMultiple}>
            {atendentes.map(atendente =>(
              <MenuItem key={atendente.id} value={atendente.nome}>
                {atendente.nome}
              </MenuItem>
            ))}
          </Select> */}
          {/* <div>atendente</div> */}
          {/* <TextField id="atendente" label="atendente" type="text" value={this.state.email} onChange={this.handleChangeAtendente}/> */}
        {/* <Select/> */}
          {/* <input id="atendente" placeholder="Digite o nome do serviço" type="text"/> */}
        </div>

        <div>
          {/* <div>data de inicio</div> */}
          <TextField id="dataInicio" label="data de inicio" type="text" value={this.state.email} onChange={this.handleChangeDataInicio}/>
          {/* <input id="dataInicio" placeholder="Digite a descrição do serviço" type="text"/> */}
        </div>

        <div>
            <InputLabel style={{margin:'10px'}}>escolha um ou mais serviços</InputLabel>

            <Select
              id="servicos"
              value={servSelecionado}
              onChange={this.handleChangeServicos}>
              {servicos.map(servico => (
                <MenuItem key={servico.id} value={servico.id}>
                  {servico.nome}
                </MenuItem>
                ))}
            </Select>
          </div>

        <div>
          {/* <div>data final</div> */}
          <TextField id="dataFinal" label="data final" type="text" value={this.state.email} onChange={this.handleChangeDataFinal}/>
          {/* <input id="dataFinal" placeholder="Digite a duração do serviço" type="text"/> */}
        </div>

        <div>
          {/* <div>avaliacao</div> */}
          <TextField id="avaliacao" label="avaliação" type="text" value={this.state.email} onChange={this.handleChangeAvaliacao}/>
          {/* <input id="avaliacao" placeholder="Digite a duração do serviço" type="text"/> */}
        </div>

        <div>
          <small id="emailAjuda" className="form-text text-muted">**OBS: Não compartilharemos suas informações**</small>
        </div>

        <div>
        <Button variant="contained" style={{margin:'10px'}} onClick={this.props.history.goBack} color="default">Voltar</Button>
        <Button variant="contained" style={{margin:'10px'}} onClick={this.salvarAgenda} color="default">Confirma</Button>
          {/* <button style={{margin:'10px'}} onClick={this.login}>Continuar</button>
          <button style={{margin:'10px'}} onClick={this.signUp}>Voltar</button> */}
          <Footer logoutpath='/' />
        </div>
      </div>
      </>
    );
  }
}
export default Servico;