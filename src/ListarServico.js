import React from 'react';
import Footer from './components/footer/Footer.js'
import db from './config/FireStoreConfig'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Menu from './components/menu/Menu' 
import { List, ListItem } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom'

export default class Servico extends React.Component{
  constructor(props) {
  super(props)
  this.state={
    listServico: [],
  }
  }

  componentDidMount() {
    let resultado =[]
    db.collection('servicos').get()
    .then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
        resultado.push({...doc.data(), id:doc.id})
      })
    })
    .then(() => {
      this.setState({...this.state, listServico : resultado})
      console.log(resultado)
    })
  }
   
  handleChangeNome = (e) => {
    this.setState({...this.state, nome : e.target.value})
  }
      
  handleChangeDescricao = (e) => {
    this.setState({...this.state, descricao : e.target.value})
  }
      
  handleChangeDuracao = (e) => {
    this.setState({...this.state, duracao : e.target.value})
  }
    
  render() {
    const {listServico} = this.state
    return (
      <>
      <Menu tela='Lista de Servicos'/>
      <div style={{textAlign:'center'}}>
      
      {/* <List component="nav">
        {listServico.map(servlist =>(
          <ListItem key={servlist.id} value={servlist.id}>
            {servlist.nome}
          </ListItem>
        ))}
      </List> */}

      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Nome</TableCell>
              <TableCell>Descrição</TableCell>
              <TableCell>Duração</TableCell>
              <TableCell>Ações</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {listServico.map(servlist => (
              <TableRow key={servlist.id}>
                <TableCell>{servlist.nome}</TableCell>
                <TableCell>{servlist.descricao}</TableCell>
                <TableCell>{servlist.duracao}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
  );
      
      <div id="botao1">
      <Button component={Link} to="/Servico" variant="contained" style={{margin:'10px'}} color="primary">Novo</Button>
      {/* <Link to="/Servico">Servico</Link> */}
      </div>

        {/* <button style={{margin:'10px'}} onClick={this.login}>Continuar</button>
        <button style={{margin:'10px'}} onClick={this.signUp}>Voltar</button> */}
        <Footer logoutpath='/'/>
      </div>
    </>
    );
  }
}
